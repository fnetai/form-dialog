import React from 'react';

import Layout from "@fnet/react-layout-asya";

export default (props) => {
  return (
    <Layout {...props} />
  );
}